import json, os, subprocess

if __name__ == "__main__":

    with open("variants.json") as f:
        variants = json.load(f)

    dirs = ["train_data","val_data"]

    for dir in dirs:
        for file in os.listdir(dir):
            if file[0] in variants.keys():
                subprocess.run([
                    "mv",
                    os.path.join(dir,file),
                    os.path.join(dir,file.replace(file[0],variants[file[0]]))
                ])
