import numpy as np
import cv2
import sys, os
import argparse

from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter

def elastic_transform(image, alpha, sigma, random_state=None):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
       source:
       https://gist.github.com/chsasank/4d8f68caf01f041a6453e67fb30f8f5a
    """
    assert len(image.shape)==2

    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape

    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha

    x, y = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing='ij')
    indices = np.reshape(x+dx, (-1, 1)), np.reshape(y+dy, (-1, 1))

    return map_coordinates(image, indices, order=1, mode="constant", cval=255).reshape(shape)

def show(name,img):
    cv2.imshow(name,img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def pepper(img,color,amount,padding=0):
    num_pepper = np.ceil(amount * img.size)
    coords = [np.random.randint(padding, i-1, int(num_pepper)) for i in np.subtract(img.shape,(padding,padding))]
    img[tuple(coords)] = color

def add_patches(img,number,size,brightness,padding):
    coords = [np.random.randint(padding, i-1, int(number)) for i in np.subtract(img.shape,(padding,padding))]
    patches = np.zeros(img.shape,dtype="uint8")
    for x,y in zip(*coords):
        patches = cv2.rectangle(patches,(x,y),(x+size,y+size),brightness,-1) # -1 to fill instead of drawing outline
    patches = cv2.blur(patches,(15,15))
    # show("patches",patches)
    # cv2.imwrite("img7.png",patches)
    # add patches to img wherever this wouldn't cause integer overflow
    return np.where(img<=255-patches,img+patches,255)

def augment(char_img,morph_its,blur_kernel_size,brightness):

    # resizing
    content_dim = (100,100)
    padding = np.random.randint(15,30)
    img = char_img.copy()
    img = cv2.resize(img, content_dim)

    # padding but random = random translation
    total_padding = padding*2
    top_padding = np.random.randint(14,total_padding-15)
    btm_padding = total_padding-top_padding
    left_padding = np.random.randint(14,total_padding-15)
    right_padding = total_padding-left_padding
    img = cv2.copyMakeBorder(
        img,
        top_padding,
        btm_padding,
        left_padding,
        right_padding,
        cv2.BORDER_CONSTANT,
        value=255
    )

    # show("img",img)
    # cv2.imwrite("img1.png",img)

    # pepper + morphology = nice
    pepper(img,0,0.01*morph_its)
    # show("img",img)
    # cv2.imwrite("img2.png",img)
    open_close_kernel = np.ones((2,2),np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN,  open_close_kernel, iterations=morph_its, borderType=cv2.BORDER_REPLICATE)
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, open_close_kernel, iterations=morph_its, borderType=cv2.BORDER_REPLICATE)
    # show("img",img)
    # cv2.imwrite("img3.png",img)

    # make all lines thicker
    img = cv2.erode(img,np.ones((3,3)))
    # show("img",img)
    # cv2.imwrite("img4.png",img)
    # extract vertical lines and blur background separately
    vertical_lines = cv2.dilate(img.copy(),np.ones((7,1)))
    # show("vlines",vertical_lines)
    # cv2.imwrite("img5.png",vertical_lines)
    img = cv2.erode(img,np.ones((3,3)))
    img = cv2.blur(img,(10,10))
    # show("img",img)
    # cv2.imwrite("img6.png",img)
    # add patches of increased brightness in background
    img = add_patches(img,10,12,150,padding)
    # show("img",img)
    # cv2.imwrite("img8.png",img)
    # join vertical lines and background back together
    img = cv2.bitwise_and(img,vertical_lines)
    # show("img",img)
    # cv2.imwrite("img9.png",img)

    # brighten / darken
    if brightness < 0:
        darkening = -brightness # brightening by -50 equals darkening by 50
        img = np.where(img<255,np.where(img>darkening,img-darkening,0),img)
    else:
        img = np.where(img<=255-brightness,img+brightness,255)

    # blur
    img = cv2.blur(img,(blur_kernel_size,blur_kernel_size))

    # linearly rescale from [min,max] to [0,255]
    img = np.uint8((img-img.min()) / (img.max()-img.min()) * 255)

    # show("img",img)
    # cv2.imwrite("img9a.png",img)

    # distort
    alpha = 30 # bigger alpha => distort more
    sigma = 8 # bigger sigma => bigger radius of gausian distortions
    img = elastic_transform(img,alpha,sigma)

    # show("img",img)
    # cv2.imwrite("img9b.png",img)
    return img

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_dir", required=True)
    parser.add_argument("-o", "--output_dir")
    args = parser.parse_args()

    count = 0
    file_list = os.listdir(args.input_dir)
    for file_name in file_list:
        for i in range(2):
            img = cv2.imread(os.path.join(args.input_dir,file_name), cv2.IMREAD_GRAYSCALE)
            morph_its = np.random.randint(1,3) # 1 or 2
            blur_kernel_size = np.random.randint(8,18)
            brightness = np.random.randint(-100,50)
            aug_img = augment(img,morph_its,blur_kernel_size,brightness)
            cv2.imwrite(os.path.join(args.output_dir,f"{file_name[:4]}-m{morph_its}-bl{blur_kernel_size}-br{brightness}.png"),aug_img)
        # for morph_its in range(1,3):
        #     for blur_kernel_size in range(8,18):
        #         for brightness in [-100,-50,0,50]:
        #             img = cv2.imread(os.path.join(args.input_dir,file_name), cv2.IMREAD_GRAYSCALE)
        #             # morph_its = np.random.randint(1,4) # 1, 2 or 3
        #             # blur_kernel_size = np.random.randint(8,15)
        #             aug_img = augment(img,morph_its,blur_kernel_size,brightness)
        #             if args.output_dir:
        #                 cv2.imwrite(os.path.join(args.output_dir,f"{file_name[:4]}-m{morph_its}-bl{blur_kernel_size}-br{brightness}.png"),aug_img)
        #                 count += 1
        #                 print(f"wrote {count} images to {args.output_dir}", end="\r")
        #             else:
        #                 show(f"m{morph_its}-bl{blur_kernel_size}-br{brightness}",aug_img)
