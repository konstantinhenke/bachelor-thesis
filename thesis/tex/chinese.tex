\chapter{Chinese Writing, Fonts and Character Encoding}
\label{chapter:chinese}

In order for the reader to be able to follow along during the next chapters, it is imperative to have a basic understanding of how written Chinese models the vernacular language and functions as a morphosyllabic system. Furthermore, this chapter will also touch upon Chinese character encoding in information technology, which is needed for understanding how the \textsc{OCR} classifier works.

\section{Written Vernacular Chinese}

When in non-scientific contexts one casually speaks about ``Chinese" as a language, one usually refers to one of the two following concepts: 1. the entirety of the Sinitic languages spoken in Greater China as a branch of the Sino-Tibetan language family, including all its varieties and dialects, or 2. Mandarin Chinese, the most-spoken Sinitic language whose Beijing dialect was standardized and adopted as the national language of the People's Republic of China. The written language used throughout the later publishing years of the \textit{Jīngbào} is one that closely mirrors the vernacular Mandarin dialects and thus mostly adheres to this national standard.%
\footnote{Before the \textit{May 4th Movement} in 1919 (and, to some extend, even later), most formal writing still used Classical Chinese, which is based on Old Chinese syntax. Starting from the Qin dynasty (221 BC), spoken Chinese evolved away from its written counterpart to such an extent that Classical Chinese is now largely unintelligible to modern Chinese speakers not educated in it. However, Classical Chinese reading (not writing) is an essential part of compulsory primary and secondary education in China, Hong Kong and Taiwan, and even nowadays a certain amount of fixed classical expressions are used throughout formal writing. Most notably, \songfont{成語} \textit{chéngyǔ}, four-character classical idioms, are found in everyday use---both written and spoken.}
With the notable exception of Cantonese (mainly spoken in Hong Kong), most other Sinitic languages do not have a unitary standardized writing system%
\footnote{However, efforts are made for many Chinese languages such as Taiwanese Hokkien, a variety of Southern Min spoken in Taiwan  (cf. also \citet{lin99} and \citet{lua08} (in Chinese)). The Taiwanese Ministry of Education has published a standard character set for recommended use in Hokkien writing, titled \songfont{臺灣閩南語推薦用字} \textit{(Taiwanese Southern Min Recommended Characters)}.}%
, and while there is ongoing research on the representation of writing in other Chinese varieties in the \textit{Jīngbào}, this work will solely focus on written vernacular Mandarin.

\section{Chinese Characters}

\subsection{Characters as Morphemes}
\label{subsec:morphemes}

Chinese---with Mandarin in particular---features a writing system based on an extraordinarily extensive set of characters. \citet{wilkinson2018chinese} calls it morphosyllabic, meaning
$$\text{1 spoken syllable} = \text{1 character} = \text{1 morpheme,}$$
where what we understand as a ``word" is usually comprised of one or two (in some cases more) morphemes. The most frequently used pronouns, verbs and prepositions are usually monosyllabic, like \songfont{我} \textit{wǒ} ``I"/``me" or \songfont{去} \textit{qù} ``to go", while most nouns are disyllabic and---contrary to many multisyllabic words in European languages---often make sense as compound words joining the meaning of their component syllables:
\begin{itemize}
\setlength\itemsep{0em}
    \item \songfont{眼鏡} \textit{yǎnjìng} ``glasses" $=$ \songfont{眼} \textit{yǎn} ``eye" $+$ \songfont{鏡} \textit{jìng} ``lens/glass";
    \item \songfont{手機} \textit{shǒujī} ``mobile phone" $=$ \songfont{手} \textit{shǒu} ``hand" $+$ \songfont{機} \textit{jī} ``machine";
    \item \songfont{冰箱} \textit{bīngxiāng} ``fridge" $=$ \songfont{冰} \textit{bīng} ``ice" $+$ \songfont{箱} \textit{xiāng} ``case/trunk/box".
\end{itemize}
This feature will be relevant for Section \ref{subsec:ocr_postprocessing}.

\subsection{Characters as Components of Other Characters}
\label{subsec:components}

To understand the challenge that comes with applying \textsc{OCR} systems to Chinese text, one also has to be aware of the low visual intra-class variance of the characters. This arises from the fact that character components reoccur in multiple---sometimes hundreds of---characters, especially in so-called phono-semantic compounds \citep[ch. 2.3]{wilkinson2018chinese}:

Looking at the character \songfont{箱} \textit{xiāng} ``box" again, it can be decomposed as \songfont{\char"2EAE} $+$ \songfont{相}. The former component is an abbreviated version of \songfont{竹} \textit{zhú} ``bamboo", which hints at the meaning of \songfont{箱}~\textit{xiāng} \underline{\smash{``box"}}. The latter (\songfont{相}) is also pronounced \textit{xiāng} and hints at the pronunciation of \songfont{箱}~\underline{\smash{\textit{xiāng}}} ``box". This way of adding meaning components (like \songfont{\char"2EAE}) to a character (like \songfont{相}) to create a new character with different meaning but similar or equal pronunciation (like \songfont{箱}) is the fundamental concept to how the vast majority of Chinese characters are composed. As a consequence, many characters share the same components, especially those within one phonetic series (a set of characters created from the same phonetic component), like \songfont{箱}~\textit{xiāng}, \songfont{廂}~\textit{xiāng}, \songfont{霜}~\textit{shuāng}, \songfont{想}~\textit{xiǎng}, \songfont{葙}~\textit{xiāng}, etc. Furthermore, this phenomenon can be of recursive nature, as in \songfont{礴}~\textit{bó}, which is composed of a semantic \songfont{石} and a phonetic \songfont{薄}~\textit{bó/báo}, which itself features a semantic \songfont{艹} and a phonetic \songfont{溥}~\textit{pǔ/bó}, which again is \songfont{氵} $+$ phonetic \songfont{尃}~\textit{fū}, which is phonetic \songfont{甫}~\textit{fǔ} $+$ semantic \songfont{寸} (cf. e.g. \citet{zhengzhang2003old}).

While it is difficult to exactly specify the percentage of phono-semantic compounds in all of Chinese characters, it is obvious that the vast majority of characters consist of reoccurring parts---no matter if in a phonetic, semantic or ideographic function \citep[Ch. 2.3]{wilkinson2018chinese}. This results in a high visual confusability and proposes a big challenge for Chinese \textsc{OCR} systems, in contrast to systems processing Latin-alphabet-based documents.

\subsection{Unicode and Fonts}
\label{subsec:unicode_fonts}

With the rise of machine-processed text arose the need for suitable character encoding. In alphabetic writing, this involves nothing other than the minor step of assigning a number (usually called ``code point") to every letter, even though occasionally, the visually same glyph is mapped to multiple code points across different alphabets, such as \textsc{latin capital letter a} (\texttt{U+0041}), \textsc{cyrillic capital letter a} (\texttt{U+0410}) and \textsc{greek capital letter alpha} (\texttt{U+0391}) which in most fonts should look exactly the same: A, Α, Α. That is, these are not the same \textit{graphemes}---abstract functional units of writing---although the \textit{glyphs}---their visible surface forms---look the same. Opposingly, ``ɑ" would usually be perceived as the same grapheme as ``a"\footnote{Usually known from handwritten text or italic printing (where a → \textit{a}), since ``ɑ" is not commonly seen in non-italic printing and has its own meaning when used as a character of the \textsc{IPA}.}, though these are two different glyphs. Different glyphs representing the same grapheme are also called allographs.

\begin{savenotes}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/Han_unification_example_返.png}
    \vspace{-1em}
    \caption[Allographs of the grapheme \songfont{返}]{Regional glyph variations (allographs) of the grapheme \songfont{返}\footnote{Source: \url{https://w.wiki/3s9f}}}
    \label{fig:han_unification}
\end{figure}
\end{savenotes}

Unicode treats characters based on the principle of assigning code points to graphemes, not to glyphs; hence the creator of a font must decide upon the exact placement, angle, thickness etc. of every stroke when designing a glyph for a certain code point. This poses a problem when dealing with Chinese characters: Fig. \ref{fig:han_unification} shows an example of variations in the glyph representations of the grapheme \songfont{返} across various regions. In the course of the so-called \textit{Han unification} \citep{allen12}, all of the allographs in Fig. \ref{fig:han_unification} have been assigned the same code point, leaving it up to the font designer to choose a standard to follow, and at the same time making it impossible for anyone to type a specific variant without the right font at hand. Some of the unified%
\footnote{= considered the same abstract grapheme and thus encoded at the same code point}
variants are quite considerable in their structural dissimilarity, such as {\songtw 直} (PRC/ROC standard) vs. {\songjp 直} (Japanese standard) which are both encoded at \texttt{U+76F4}. Inconsistently, their descendants \songfont{值} and \songfont{値} were each given their own code point (\texttt{U+503C} and \texttt{U+5024}, respectively).%
\footnote{Hence, I have to used two different fonts to type out {\songtw 直} vs. {\songjp 直} but can use a single font for \songfont{值} and \songfont{値} as long as it provides glyphs for both code points.} At the time of writing, Unicode provides code points for 93,779 characters (Table \ref{tab:unicode_blocks}).

\begin{figure}[t]
    \centering
    \includegraphics[width=0.55\textwidth]{img/heisongkai.png}
    \caption[Most common Chinese font styles]{The most common Chinese font styles (left to right):\\[0.3em]\songfont{黑體} \textit{hēitǐ} (sans-serif / gothic);\newline\songfont{明體} \textit{míngtǐ} (Ming) a.k.a. \songfont{宋體} \textit{sòngtǐ} (Song);\newline\songfont{楷書} \textit{kǎishū} (regular script);\newline\songfont{隸書} \textit{lìshū} (clerical script)}
    \label{fig:font_styles}
\end{figure}

As for font styles, most of Chinese text elements are covered by three main styles (see Fig. \ref{fig:font_styles}): Sans-serif and Ming/Song can be seen as the direct equivalent of sans-serif and serif fonts known for Latin-based typography, where the former is more commonly seen on websites and (in variations) on posters etc. with shorter textual elements, while the latter is used for printing longer continuous texts as found in books and newspapers. Regular script is closer to handwritten character shapes, but still strictly standardized in terms of stroke length and position, hence it is largely used in language teaching material. It is also commonly seen as an equivalent to italic printing in Latin-based texts, as simply ``obliquing" Chinese characters is a rather obvious typographic faux pas.

A fourth style, the clerical script, originates from Han-dynasty calligraphy, but since it is still highly legible to modern readers, it can still be found wherever some kind of artistic flavor or antique and classical appeal is strived for.

All of the printing in the \textit{Jīngbào}'s text blocks is done in a Song font.

\begin{table}[t]
    \centering
    \begin{tabular}{llr}
    \toprule
        Block range & Block name & \parbox[c][10mm][c]{18mm}{\raggedright number of characters} \\
        \midrule
         \texttt{U+2E80\dots U+2EFF} &	CJK Radicals Supplement & 	115 \\
         \texttt{U+2F00\dots U+2FDF} &	Kangxi Radicals & 	214  \\
         \midrule
         \texttt{U+3400\dots U+4DBF} &	CJK Unified Ideographs Extension A 	 	& 6,592 \\
         \texttt{U+4DC0\dots U+4DFF} &	Yijing Hexagram Symbols & 	64 \\
         \texttt{U+4E00\dots U+9FFF} &	CJK Unified Ideographs & 	20,989  \\
         \midrule
         \texttt{U+20000\dots U+2A6DF} &	CJK Unified Ideographs Extension B & 	42,718 \\
         \texttt{U+2A700\dots U+2B73F} &	CJK Unified Ideographs Extension C 	 	& 4,149 \\
         \texttt{U+2B740\dots U+2B81F} &	CJK Unified Ideographs Extension D & 	222 \\
         \texttt{U+2B820\dots U+2CEAF} &	CJK Unified Ideographs Extension E 	 	& 5,762 \\
         \texttt{U+2CEB0\dots U+2EBEF} &	CJK Unified Ideographs Extension F 	 	& 7,473 \\
         \texttt{U+2F800\dots U+2FA1F} &	CJK Compatibility Ideographs Supplement & 	542 \\
         \texttt{U+30000\dots U+3134F} &	CJK Unified Ideographs Extension G 	 	& 4,939  \\
         \midrule
         & & \textbf{Σ\hspace{0.5em}93,779} \\
         \bottomrule
    \end{tabular}
    \caption[CJK characters in Unicode]{CJK (Chinese, Japanese, Korean) characters in Unicode}
    \label{tab:unicode_blocks}
\end{table}

\subsection{Traditional vs. Simplified Chinese}
\label{subsec:trad_simp}

There are two standardized character sets for modern written Chinese: Traditional and simplified characters. The latter were promoted by the People's Republic of China's government for official use in printing, writing and education since the 1960s under the expectation of increasing literacy in the population. They are now in official use in the \textsc{PRC}, Malaysia and Singapore, whereas traditional Chinese remains in use in Taiwan, Hong Kong and Macau \citep{chu2012chinese}. Simplification was done with the ultimate goal of reducing written strokes, which was approached by various methods:
\begin{enumerate}
\setlength\itemsep{0em}
    \item modifying existing cursive shapes for use in printing, e.g. \songfont{書} → \songfont{书}, \songfont{樂} → \songfont{乐};
    \item merging homophones, e.g. \songfont{復}/\songfont{複}/\songfont{覆}/\songfont{复} → \songfont{复} (all pronounced \textit{fù});
    \item omitting entire components, e.g. \songfont{廣} → \songfont{广}, \songfont{飛} → \songfont{飞}, \songfont{習} → \songfont{习}, \songfont{滅} → \songfont{灭};
    \item replacing phonetic components by others that have less strokes: \songfont{鄰} \textit{lín} → \songfont{邻} \textit{lín} (with the phonetic components \songfont{粦} \textit{lín} → \songfont{令} \textit{lìng});
    \item re-adopting obsolete ancient variants or adopting variants already in use:\newline \songfont{從} → \songfont{从}, \songfont{眾} → \songfont{众}, \songfont{網} → \songfont{网}, \songfont{與} → \songfont{与}, \songfont{卻} → \songfont{却}; \newline\vspace{2mm}etc.
\end{enumerate}
Point 5 is particularly relevant to working with the \textit{Jīngbào}: The parts of the image corpus I will use for the methods described in Chapter \ref{chapter:implementation} are from April 1939 and hence all printed before the government introduced the simplified character set, however some characters are already printed in a form later adopted during the simplification. For instance, the \textit{Jīngbào} prints \songfont{幇}, \songfont{强}, \songfont{温} and \songfont{却} while the traditional characters used in the modern Taiwanese standard are \songfont{幫}, \songfont{強}, \songfont{溫} and \songfont{卻}, respectively (cf. Table \ref{tab:variant_chars} on \pageref{tab:variant_chars}).

The absolute number of traditional characters that were simplified is hard to tell, as simplification is applied recursively: \songfont{馬} (``horse") is simplified to \songfont{马}, which is then further conducted at component level in \songfont{媽}, \songfont{碼}, \songfont{騎}, \songfont{駛}, \songfont{驗}\footnote{i.e. in simplified Chinese, \songfont{妈}, \songfont{码}, \songfont{骑}, \songfont{驶}, \songfont{验} etc. are used} etc. There is, however, a considerable number of trivially simplifiable characters not in modern use like \songfont{騿}%
\footnote{According to \url{zdic.net}, a name of a horse. Presumably from some pre-modern work of literature.}%
, for which Unicode only provides one code point for the traditional form. This is to say, recursive simplification is not done for every traditional character encoded in Unicode, and as soon as one leaves the boundaries of Unicode, Chinese characters become very difficult to quantify. To still provide a reference point on the impact of character simplification: Around 29\% of the characters in the ground truth section presented in Chapter \ref{chapter:implementation} have a different form in the two character sets, not excluding multiple occurrences of the same character.
