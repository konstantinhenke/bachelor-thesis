\chapter{Related Work}
\label{sec:related_work}

While at some point in the future, the ultimate goal for historical document processing will be to create a single pipeline that takes any document image as an input and is able to output all content information in a suitable format such as \textsc{PAGE-XML} \citep{pletschacher10}, at the moment most research is still focused on producing efficient approaches for smaller substeps. This chapter is dedicated to giving exemplary insight into this. 

\section{Document Image Segmentation}

Even though this thesis focuses solely on the character segmentation and recognition steps following below, one should not neglect the necessity for accurate page segmentation techniques. A survey presented by \citet{eskenazi17} gives a comprehensive overview on approaches to these challenges. Generally, one can differentiate between techniques that work top-down (starting from the entire page, segmenting it into its components) and those that work bottom-up (finding components that belong together to aggregate them). Their paper presents a detailed collection of both classical and deep learning algorithms, where the former often achieve surprisingly competitive results, although they rely on strict assumptions about various layout elements. Within the realm of the \textsc{ECPO} project, the historical nature of the document scans poses additional challenges. However, first bottom-up segmentation experiments using morphology-based methods as described by \citet{liu10} yield acceptable results.\footnote{\url{https://github.com/exc-asia-and-europe/ecpo-full-text/wiki/Finding-and-Connecting-Separators}}

On the other hand, methods involving neural networks obviously require a sufficient amount of accordingly annotated data but allow for less strict layout assumptions, as shown in \citet{kai17}, or the \textit{dhSegment} tool \citep{oliveira18}, who treat page segmentation as a pixel labeling problem. Recently, more tools like \textit{eynollah}\footnote{\url{https://github.com/qurator-spk/eynollah}} (part of the \textit{Qurator} project \citep{rehm2020qurator}) allow for straightforward training and even out-of-the-box usage. As for \textsc{ECPO}, first results using \textit{dhSegment} look promising as well.\footnote{\url{https://github.com/exc-asia-and-europe/ecpo-segment}}

\section{Chinese Character Detection and Segmentation}
\label{sec:detec_segment}

Once a document image is segmented into smaller structurally connected units, the next step is to detect the textual elements. One has to be aware of the fact that especially for Chinese characters, detection/segmentation and recognition are trivially separable steps, so it's reasonable to seek independent optimization. While Chinese characters can generally appear in all different kinds of fonts and styles \citep{yuan2018chinese}, in printed text, character shapes are never connected to each other and their squared appearance often yields an implicit grid layout. This allows for classical non-neural segmentation techniques using projection profiles \citep{fan1998classification,lin2001chinese}. \citet{mei2013chinese} also build on projection profiles and leverage their use for single lines with connected component analysis, as do \citet{xu2017chinese} as well as \citet{van2011development} for Vietnamese chữ Nôm%
\footnote{a logographic writing system based on Chinese characters formerly used for the Vietnamese language}
characters. In addition to character segmentation for grid-layout text blocks, projection profiles can also be used for skew detection and correction \citep{li2007skew}.

It is out of question, though, that writing style, skew, noise or other issues concerning document image quality oftentimes produce layout situations that hamper these rule-based approaches, even to a degree that renders them completely unusable, e.g. for unconstrained handwriting or even calligraphy. Fortunately, thanks to deep learning in computer vision, object detection has overcome the problem of fixed hyperparameters. Generally speaking, U-Net \citep{ronneberger2015unet} has become popular for segmenting text areas and object detection models like the anchor-based \textsc{YOLO} \citep{redmon2018yolov3} to find the exact bounding boxes. \citet{yang2018dense} employ what they call a recognition guided proposal network (\textsc{RGPN}) after segmenting single columns using projection profiles to propose regions of characters, and another \textsc{CNN}-based detection network to obtain their precise bounding boxes. \citet{tang2020hrcenternet} propose HRCenterNet\footnote{\url{https://github.com/Tverous/HRCenterNet}}, an anchorless object detection network.

Finally, there do exist all-in-one solutions like \textit{Tesseract}\footnote{\url{http://code.google.com/p/tesseract-ocr}} \citep{smith2007overview} that apart from line finding also provide \textsc{OCR} functionality including language-model-based correction algorithms. \citep{ma2020joint} also present a framework that can jointly conduct layout detection, character detection and recognition.

\section{Chinese Character Recognition}

This section is about the actual \textsc{OCR} step. Depending on whether one deals with printed or handwritten Chinese characters, academic literature also often refers to the recognition step as \textsc{PCCR} (printed Chinese character recognition) and \textsc{HCCR} (handwritten Chinese character recognition). While for obvious reasons the former is more relevant for dealing with the \textit{Jīngbào} image data, the latter has received more attention throughout the past few decades (cf. related work in \cite{melnyk2020high}), mainly due to the fact that individual handwriting styles pose a greater challenge to \textsc{OCR} techniques than the rather low variability in standard fonts used in printing. \citet{zhong2015multi} also bring up the fact that there exist more database resources for \textsc{HCCR} (e.g. \cite{liu2011casia}). Regardless of this, most methods presented in either field are generally applicable to both.

\subsection{Early Work on \textsc{PCCR}}

Probably the earliest approach to \textsc{PCCR} is the one of \citet{casey1966recognition}, who employ simple template matching techniques. One of the authors later published a ``Twenty-five Year Retrospective" on the field \citep{nagy1988chinese} in which he describes the need for ``special templates to detect radicals for subgrouping", referring to the component-assembled nature of Chinese characters. Considering reoccurring components to recognize Chinese characters intuitively does seem reasonable, however with the use of artificial neural networks (\textsc{NN}), it becomes more straightforward to simply regard characters as independent output classes -- regardless of any shared components.\footnote{There does exists more recent work concentrating on component structure on a sub-character level \citep{wang2017radical,zhang2018radical,he2018open}.} \citet{xu1992printed} were among the early pioneers to employ \textsc{NN}-based techniques for \textsc{PCCR}, followed by other approaches using traditional feedforward \textsc{NN}s \citep{khawaja2006recognition,khawaja2006classification}. \citet{dai2007chinese} give a great overview on the early stages of Chinese \textsc{OCR} before the use of \textsc{CNN}s).

\subsection{Convolutional Neural Networks}
\label{subsec:convnets}

Convolutional neural networks (\textsc{CNN}s) are inspired by the receptive fields found in animal vision and try to emulate the neurons in animals' visual cortices \citep{fukushima1982neocognitron}. Consequently, they have been used with great success in many different subfields of computer vision, specifically image recognition and classification. \textsc{CNN}s date back to \citet{lecun1989backpropagation}, who were the first to train the weights of a convolutional kernel (Fig. \ref{fig:conv_filter}) using the backpropagation algorithm%
\footnote{Independently discovered multiple times, but usually attributed to \citet{rumelhart1986learning}.}
known from traditional \textsc{NN}s. \citeauthor{lecun1989backpropagation}'s use case also happened to be \textsc{OCR}, specifically digit recognition: Fig. \ref{fig:lenet-5} illustrates the digit image as an input, the convolutional layers at the bottom and the ten output units (one for every digit) at the top.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/cnn.png}
    \vspace{-1em}
    \caption[Example for a convolutional filter with arbitrary numbers]{Example for a convolutional filter (purple) with arbitrary numbers (1--9). A \textsc{CNN} optimizes the values itself through automated learning. Image as presented by \citet{reynolds2019cnn}.}
    \label{fig:conv_filter}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/lecun_LeNet-5.png}
    \caption{The \textsc{CNN} architecture presented by \citet{lecun1989backpropagation}}
    \label{fig:lenet-5}
\end{figure}

Throughout the following decades, the basic principle of learning multiple kernels per layer and thus reducing dimensions until a fully connected layer predicts the output class largely stayed the same, though networks became deeper and added more attributes such as max pooling layers, \textsc{R}e\textsc{LU} activation and dropout, as seen e.g. in AlexNet \citep{krizhevsky2012imagenet}. AlexNet won the 2012 \textit{ImageNet Large Scale Visual Recognition Challenge} (\textsc{ILSVRC}) \citep{russakovsky2015imagenet}, and in the following years, any other architectures continued to be outperformed by deeper and more sophisticated \textsc{CNN}s, such as ZFNet \citep{zeiler2014visualizing}, an improved version of AlexNet and winner of \textsc{ILSVRC} 2013, and GoogLeNet \citep{szegedy2014going}, winner of \textsc{ILSVRC} 2014.

Following their great success in image recognition, these \textsc{CNN}s have subsequently been employed in \textsc{OCR}, e.g. both \citet{zhong15} and \citet{xu2017chinese} use a (slightly modified) GoogLeNet with great success. Motivated by these approaches and the demonstration of GoogLeNet's superiority in Chinese \textsc{OCR} as demonstrated in \citet{yuan2018chinese}, this thesis will rely on a GoogLeNet as well (cf. Section \ref{subsec:nn_architecture}). The \textsc{ILSVRC} did continue until 2017, and ever more powerful \textsc{NN}s have been presented in recent years for various vision tasks such as image classification and object detection. New state-of-the-art results on ImageNet are achived using transformer architectures known from \textsc{NLP} \citep{zhai2021scaling,dosovitskiy2021image}, which however is far beyond the scope of this thesis.

\subsection{Artificial Image Generation and Image Augmentation}

In machine learning, the training set is desired to independently represent the input and output space ideally in identical distribution to the test set. This results in the need to generate additional data if the training set would else not be of sufficient size or desired distribution, and the creators of competition-winning classifiers like AlexNet and GoogleNet all employed methods to artificially increase the number of training samples.

Chinese \textsc{OCR} generally faces a problem of data shortage due to the fact that a large number of input samples and their corresponding label in the output space for each of some thousand output classes are needed in order to model the target distribution. While suitable datasets for \textsc{HCCR} \citep{liu2011casia} exist, it is often cumbersome to obtain this amount of properly annotated printed character image data. This is where two techniques come into play: 
\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item producing entirely synthetic training images from scratch to increase the \textit{size} of the training set;
    \item augmenting existing training images to obtain a more \textit{diverse} training set.
\end{enumerate}
Augmentations often include geometric transformations, which themselves can be divided into affine transformation (preserving lines and parallelism but not necessarily distances and angles, such as translations, rotations, shearing, etc.) and elastic transformations or non-linear distortions (not generally preserving lines and parallelism). One of the significant improvements in classifiers trained for handwritten digit recognition on the MNIST dataset has been obtained by the addition of elastic deformations \citep{simard2003best} to already existing affine transformations \citep{wong2016understanding}.

In related work, the two above steps are usually applied jointly: \citet{visapp09character} synthesize 62992 character images for the English alphabet from 254 fonts in 4 different styles and \citet{jaderberg2014synthetic} produce images of entire English words using an elaborate generation procedure. With regard to Chinese, \citet{ren2016cnn} generate character images from 32 fonts and present their entire augmentation pipeline:
\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item random selection of character and background color,
    \item adding borders and/or shadows,
    \item distorting the characters using projective transformations,
    \item adding scene background patches to imitate real-word reflections,
    \item adding Gaussian noise and Gaussian blur with random intensity.
\end{enumerate}
Following these approaches, \citet{xu2017chinese} design a synthetic character engine that starts from clean images extracted from 28 fonts and then adds either random noise or erosion and blur. \citet{zhong2015multi} also employ non-linear transformations, demonstrating their effectiveness in increasing \textsc{CNN} performance and arguing that affine transformations are mostly unsuitable for Chinese characters.

\section{\textsc{OCR} Post-Processing for Error Correction}
\label{subsec:ocr_postprocessing}

Since predicting the right one among thousands of Chinese characters based on context-less pixel information is a notoriously difficult task, the output of any Chinese \textsc{OCR} model is likely to be subject to a considerable number of errors. Hence, there is a need for \textsc{OCR} post-processing. Languages with alphabetic writing systems allow for correction methods fundamentally different from what is needed for Chinese; for instance, \citet{kissos2016ocr} propose a method for misspelled words in Arabic \textsc{OCR}: On a word-level, they find correction candidates using Levenshtein distance, then train a classifier which decides whether or not to replace the \textsc{OCR} output with the highest ranked correction candidate with regard to \textsc{OCR} confidence, term frequency and dictionary features. Similar approaches are imaginable for English or other Indo-European languages with alphabetic writing systems.

Even though Chinese writing is fundamentally different, word-level approaches have not been entirely out of question for it, especially before powerful language models have come into existence: \citet{tseng2002error} presents an algorithm that simply clusters confusing pairs (pairs of frequently seen two- or three-character sequences that only differ in one character) together and assumes the one with higher document frequency as correct.

Ultimately, the problem of correcting an \textsc{OCR} model's wrong character predictions can be generalized to Chinese spell checking (\textsc{CSC}), which over the last years has often been addressed by simply having a language model predict the right character from a previously obtained candidate set. This is reasonable due to the fact that \textit{visually} similar characters often only differ in one of their components (cf. also Section \ref{subsec:components}), which means that characters in the candidate list are particularly unlikely to be \textit{semantically} similar and thus---intuitively---easy to be corrected by a language model that takes context into account. Generally however, it is not trivial to find a good heuristic for
\begin{enumerate}[label=\alph*)]
\setlength\itemsep{-0.3em}
    \item deciding which characters are likely to need correction at all and
    \item deciding which candidates to have the language model choose from.
\end{enumerate}
For example, \citet{zhuang2004chinese} use an n-gram model and an \textsc{LSA}\footnote{Short for ``latent semantic analysis", for further information cf. Zhuang et al.'s work mentioned above.} language model, then address problem a) by taking all characters into consideration and problem b) by generating a candidate list of visually similar characters as follows:
\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item Record $n(F,C)$ in the training corpus. $n(F,C)$ is the times that $F$ is the first choice in the candidate list produced by the \textsc{OCR} engine while the correct (``gold") character is $C$. 
    \item Calculate the confusion probability $P(C|F) = n(F,C)/n(F)$ where $n(F)$ is the times  that  $F$ is the first choice in the candidate list. 
    \item Sort all possible $C$ according to the probability.
    \item  Save the first $M$ possible characters as the similar characters of $F$,  and save their confusion probabilities.
\end{enumerate}
Apart from that, various other approaches have been proposed: \citet{zhuang2005ocr} use the \textsc{OCR} model's candidate distance instead of a fixed $k$ to both decide which characters to correct (a) and to reduce the search space (b). Then they combine various n-gram models to find the most likely candidate. Similarly, \citet{wang2019towards} manually set an \textsc{OCR} confidence threshold of 95\% (a) and have the language model choose the most likely candidate among the top 5 \textsc{OCR} candidates (b).

\textsc{CSC} is however not only needed for \textsc{OCR} post-processing (with a search space of \textit{visually} similar characters), but also for post-processing of e.g. Chinese text typed on computers with a search space of \textit{phonetically} similar characters (as Chinese character input methods are usually based on character pronunciation) and eventually most methods are applicable to both. \citet{hong2019faspell} generate the search space (b) from databases of both visually and phonetically similar characters and use a masked language model similar to \textsc{BERT} \citep{devlin2019bert} for correction. \citet{yang2019post} demonstrate a technique for character correction in speech recognition: They train a \textsc{B}i-\textsc{LSTM} model operating on single characters to identify erroneous characters (a) and then replace them with the most likely candidate from a list of homophones (b). The top candidate is predicted by another \textsc{B}i-\textsc{LSTM} model.

While the fact that most Chinese words are disyllabic (cf. Section \ref{subsec:morphemes}) allows for efficient use of n-gram models as shown above, there are plenty of situations where switching single characters (especially those that aren't part of a disyllabic word) will still result in a meaningful and grammatical sentence, e.g.
\begin{itemize}
\setlength\itemsep{-0.3em}
    \item \songfont{他不喜歡你} "he doesn't like you",
    \item \songfont{他也喜歡你} "he also likes you",
    \item \songfont{他們喜歡你} "they like you",
\end{itemize}
where only the second character is changed (\songfont{不} "not", \songfont{也} "also", \songfont{們} (plural marker)). Relying on masked language models such as \textsc{BERT} trained on single characters as tokens might therefore yield superior performance, as more context is taken into account. Hence, in this work I will employ a \textsc{BERT} language model for \textsc{OCR} post-processing.