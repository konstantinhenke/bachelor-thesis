\chapter{Experiments and Discussion}

%This chapter presents results obtained from experiments using the methods described in the chapter above. It will first give insight into the procedure for training the \textsc{OCR} classifier and resulting accuracy values for the validation set, and then proceed to explain optimization for \textsc{OCR} post-processing.

%\section{Training the \textsc{CNN}}

Training on the images generated as described in Section \ref{sec:img_augmentation} means training on data randomly augmented on the fly. As ever new images are generated with a large number of parameters for randomization, it is highly unlikely for the \textsc{CNN} to see the same input image twice. On the other hand, during training on the fixed set of previously extracted ``real-life" character images, the \textsc{CNN} sees the same set of images in every epoch. As a consequence, convergence is estimated to be considerably slower for training on the synthetic data.

Furthermore, no matter how well the synthetic images emulate real-life data, they are not likely to surpass it in terms of distribution congruence with the validation set. Consequently, training on real-life images is likely to lead to a higher accuracy than training on \textit{only} synthetic data.

From these two assumptions I derive the strategy to carry out extensive pre-training on synthetic data first, and to fine-tune the network on the real-life character images after. I conduct experiments with and without pre-training before passing the best model on to post-processing using \textsc{BERT}. Finally, using the optimal hyperparameters I evaluate the best models on the test set and discuss results.

\section{Pre-training on Synthetic Data}

I use a batch size of 4 and set up training using stochastic gradient descent as an optimizer with a fixed learning rate of 0.001 and a momentum of 0.5. One epoch equals training on those of the 4,806 classes that the individual fonts provide a glyph for. The fonts lack glyphs for between 9 (TW-Sung) and 128 (SourceHanSerif JP) of the classes, which I consider negligible as the missing characters can be safely assumed to be rare ones and the upper bound for the resulting accuracy loss during pre-training is only $128/4806=2.66\%$.

Every image is augmented using the method described on page \pageref{fig:character_synth} and then passed to the \textsc{CNN}. After every epoch, the network is evaluated on the validation set and saved with the current parameters. It is then trained for as many epochs as needed until no improvement is made over 50 epochs. The model with the highest validation accuracy is kept, all other checkpoints are discarded. The \textsc{CNN} is trained on an Nvidia \textsc{GTX} 1080 Ti. For the pre-training, one epoch takes roughly 4.2 minutes, amounting to between 24 and 48 hours for one model to finish training.

Fig. \ref{fig:val_accs} (a) shows the validation accuracies during training on the individual fonts vs. on the entire set of glyph images from all fonts. The following observations can be made:
\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item The font that performs best individually, SourceHanSerif JP, is not the one that seems the closest to the real-life data in terms of character variants (arguably I.Ming, cf. Table \ref{tab:fonts}). This suggests that contrary to intuition, the rather limited number of variants with deviant stroke positions (cf. 2. in Section \ref{sec:implfonts}) is less important for emulating the real-life characters than the general structure of the entire font's glyphs (stroke width and length, character height/width ratio, size of certain components in relation to character size etc.; 1. in Section \ref{sec:implfonts}). 
    \item Training on all fonts combined yields slight improvement over training on only the best performing font (SourceHanSerif JP). Higher peaks after some epochs yield considerably higher accuracies. These ``lucky catches" are most likely based on the strong randomization (and thus variability) of the training set.
    \item As assumed, the validation accuracy barely decreases after converging, suggesting that there is not a lot of overfitting. Longer training times might reveal a more pronounced decline, which was not tested for practical and resource reasons.
\end{enumerate}

\section{Fine-tuning on Extracted Character Images}

Using the same training settings (batch size, optimizer, learning rate, momentum) as for pre-training, I continue training on the set of 47,986 characters---once from scratch, once continuing from the maximum accuracy model yielded during pre-training on all fonts (the red dot in Fig. \ref{fig:val_accs} (a), which is equivalent to the light blue dot in Fig. \ref{fig:val_accs} (b)). This time, the training images are natural and not augmented, hence they are seen repeatedly in every epoch. As a consequence of differing epoch sizes, epochs are not a suitable measure of training time, which is why Fig. \ref{fig:val_accs} instead refers to the number of seen training samples for comparability. Fig. \ref{fig:val_accs} (b) allows for the following observations:

\afterpage{\clearpage}
\begin{figure}[t]
    \centering
\makebox[\textwidth][c]{
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=9cm]{img/pretraining.png}
        \caption{Pre-training on artificial image data}
    \end{subfigure}
    \hspace{4mm}
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=9cm]{img/combined_plot.png}
        \caption{\mbox{Training on real-life data with and without pre-training}}
    \end{subfigure}
    }
    \caption[Development of validation accuracies during training]{Development of validation accuracies during training. Maxima are marked with a larger dot, for numerical values cf. Table \ref{tab:ocr_results}. \newline ``All fonts" in (a) is equivalent to ``pretraining on all fonts" in (b).}
    \label{fig:val_accs}
\end{figure}
\begin{table}[b]
\hspace*{-1.5em}
\begin{tabular}{rr*{10}{c@{\hspace{1.5mm}}}}
\toprule
    & font name & $k=$ 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10\\ \midrule
    
    \multirow{5}{*}{\rotatebox{90}{\smash{synthetic}}} & TW-Sung & 54.40 & 64.86 & 69.51 & 72.19 & 74.18 & 75.65 & 76.79 & 77.76 & 78.57 & 79.30  \\
    
    & HanaMin A & 47.69 & 59.45 & 64.87 & 68.10 &  70.42 & 72.17 & 73.46 & 74.60 & 75.54 & 76.22 \\
    
    & SourceHanSerif JP  & \textbf{62.62} & \textbf{70.64} & \textbf{74.09} & \textbf{76.23} & \textbf{77.76} & \textbf{78.94} & \textbf{79.93} & \textbf{80.61} & \textbf{81.24} & \textbf{81.69} \\
    
    & I.Ming & 55.60 & 66.41 & 70.90 & 73.84 & 76.09 & 77.62 & 78.83 & 79.86 & 80.60 & 81.23\\[1.5mm]
    
    & ($*$) all fonts & \textbf{69.73} & \textbf{78.30} & \textbf{81.68} & \textbf{83.65} & \textbf{84.99} & \textbf{86.06} & \textbf{86.87} & \textbf{87.49} & \textbf{87.97} & \textbf{88.46} \\
    \midrule
    
    \multirow{2}{*}{\rotatebox{90}{real}} & without pre-training & 96.54 & 97.32 & 97.49 & 97.58 & 97.64 & 97.65 & 97.70 & 97.70 & 97.71 & 97.71 \\
    & after pre-tr. on ($*$) & \textbf{97.63} & \textbf{98.57} & \textbf{98.78} & \textbf{98.91} & \textbf{98.98} & \textbf{99.01} & \textbf{99.07} & \textbf{99.10} &  \textbf{99.12} & \textbf{99.13} \\
    \bottomrule
\end{tabular}
\caption{Top-$k$ accuracy of the \textsc{OCR} classifier on the validation set}
\label{tab:ocr_results}
\end{table}

\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item With pre-training, the model achieves higher accuracy. This is presumably due to the greater amount of diverse training data leading to better generalization even on the specific font in the validation data and justifies the motivation and method presented in Section \ref{sec:img_augmentation}.
    \item The model converges faster after pre-training, even when taking the time spent on pre-training into account: Once the maximum is reached, no improvement is observable for more than 4 million seen samples. The model trained on real-life data from scratch however shows very slow convergence and new slight improvements (mostly by 0.01\%) are reached over a long time.
    \item During fine-tuning, the amplitude of accuracy values between epochs is visibly lower than during pre-training, where peaks and lows are deviating stronger from each other. This can be ascribed to the randomization which leads to a training set changing between epochs, in contrast to the static real-life training set.
\end{enumerate}

\section{\textsc{OCR} Results}

Extending what can be seen in Fig. \ref{fig:val_accs}, Table \ref{tab:ocr_results} quantifies the entire results on the validation set. Most importantly, top $k$ accuracy is presented for $k = 2,\dots,10$, too, which will be important for error correction using the \textsc{BERT} model. Generally, it is evident that accuracies are substantially lower when only training on synthetic data. This is supposedly due to the fact that the feature distribution will always be worse than that of real-life data which is naturally more similar to the validation set the model is evaluated on. As stated before, the best results are achieved when pre-training on randomly augmented glyph images of all four fonts and fine-tuning on the segmented real-life character images: Pre-training raises the top-1 validation accuracy from 96.54\% to 97.63\% which is equivalent to an error reduction of 31.5\%. Final results on the test set will be presented in the next section.

\newcommand{\gr}[1]{\textcolor{gray}{#1}}
\begin{table}[b]
\centering

\begin{minipage}{\linewidth}
\hspace*{-1em}
\raisebox{-0.7mm}{
\begin{tabular}{r|*{10}{c@{\hspace{1.5mm}}}}
    \rule[-2.2mm]{0mm}{3mm} sample & \multicolumn{10}{c}{top 10 candidates} \\\hline
    \rule{0mm}{5mm}
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/骨-310-13-2.png}} &
\iMing \gr{肯} & \iMing \gr{貴} & \iMing 骨 & \iMing \gr{片} & \iMing \gr{督} & \iMing \gr{昔} & \iMing \gr{皆} & \iMing \gr{貨} & \iMing \gr{旨} & \iMing \gr{嘗}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/貽-541-11-7.png}} &
\iMing \gr{胎} & \iMing 貽 & \iMing \gr{船} & \iMing \gr{貼} & \iMing \gr{晤} & \iMing \gr{賠} & \iMing \gr{始} & \iMing \gr{販} & \iMing \gr{斯} & \iMing \gr{脂}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/汕-621-6-3.png}} &
\iMing \gr{油} & \iMing 汕 & \iMing \gr{別} & \iMing \gr{遇} & \iMing \gr{洲} & \iMing \gr{勃} & \iMing \gr{海} & \iMing \gr{前} & \iMing \gr{西} & \iMing \gr{効}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/檸-330-7-8.png}} &
\iMing \gr{棒} & \iMing \gr{梭} & \iMing \gr{慘} & \iMing \gr{核} & \iMing \gr{稼} & \iMing \gr{橡} & \iMing \gr{桂} & \iMing \gr{俸} & \iMing \gr{梓} & \iMing \gr{控}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/薄-280-11-4.png}} &
\iMing \gr{蒲} & \iMing \gr{蓮} & \iMing 薄 & \iMing \gr{通} & \iMing \gr{滯} & \iMing \gr{謝} & \iMing \gr{浦} & \iMing \gr{逝} & \iMing \gr{蕩} & \iMing \gr{鼎}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/歎-120-22-5.png}} &
\iMing \gr{數} & \iMing 歎 & \iMing \gr{歌} & \iMing \gr{欵} & \iMing \gr{歡} & \iMing \gr{欽} & \iMing \gr{教} & \iMing \gr{歉} & \iMing \gr{默} & \iMing \gr{歔}\\
\end{tabular}
}
    \hspace*{1em}
\begin{tabular}{r|*{10}{c@{\hspace{1.5mm}}}}
    \rule[-2.2mm]{0mm}{3mm} sample & \multicolumn{10}{c}{top 10 candidates} \\\hline
    \rule{0mm}{5mm}
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/閱-579-13-3.png}} &
\iMing \gr{開} & \iMing 閱 & \iMing \gr{聞} & \iMing \gr{則} & \iMing \gr{閔} & \iMing \gr{題} & \iMing \gr{圍} & \iMing \gr{四} & \iMing \gr{期} & \iMing \gr{閡}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/麼-001-19-26.png}} &
\iMing \gr{廖} & \iMing 麼 & \iMing \gr{慶} & \iMing \gr{廳} & \iMing \gr{鑒} & \iMing \gr{豐} & \iMing \gr{麽} & \iMing \gr{應} & \iMing \gr{禦} & \iMing \gr{農}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/貲-109-17-6.png}} &
\iMing \gr{買} & \iMing 貲 & \iMing \gr{貨} & \iMing \gr{質} & \iMing \gr{賞} & \iMing \gr{贊} & \iMing \gr{貸} & \iMing \gr{貿} & \iMing \gr{賢} & \iMing \gr{實}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/懿-121-8-31.png}} &
\iMing \gr{盛} & \iMing 懿 & \iMing \gr{驚} & \iMing \gr{盤} & \iMing \gr{璇} & \iMing \gr{離} & \iMing \gr{鑑} & \iMing \gr{蓬} & \iMing \gr{蹤} & \iMing \gr{蘇}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/摸-336-5-16.png}} &
\iMing \gr{換} & \iMing \gr{撲} & \iMing \gr{模} & \iMing \gr{摟} & \iMing \gr{瑛} & \iMing 摸 & \iMing \gr{漢} & \iMing \gr{搜} & \iMing \gr{樓} & \iMing \gr{揍}\\
\raisebox{-1mm}{\includegraphics[scale=0.5]{img/wrong_examples/甲-455-10-3.png}} &
\iMing \gr{申} & \iMing 甲 & \iMing \gr{中} & \iMing \gr{串} & \iMing \gr{弔} & \iMing \gr{早} & \iMing \gr{冉} & \iMing \gr{里} & \iMing \gr{叩} & \iMing \gr{官}\\
\end{tabular}
\end{minipage}

    \caption{Examples of wrong \textsc{OCR} predictions}
    \label{tab:wrongpreds}
\end{table}

As mentioned above, Table \ref{tab:ocr_results} also shows top $k$ accuracy values for $k>1$. Unsurprisingly, a higher $k$ leads to a considerably higher accuracy for all of the models. This is because for incorrect \textsc{OCR} predictions, the correct candidate is often predicted in second or third place, as can be seen in the examples in Table \ref{tab:wrongpreds}. This intuitively justifies the approach described in the next section.\vspace{1em}

\section{\textsc{OCR} Error-Correction Using a \textsc{BERT} Model}
\label{sec:exp_postproc}

\afterpage{\clearpage}
\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{img/histograms2.png}
    \caption[Logit difference between top 1 and top 2 \textsc{OCR} candidate]{Values for $x_1-x_2$ for right (a) and wrong (b) predictions and accuracy on validation set for different $t$ after \textsc{LM}-correction (c) (numeric values up to $k=18$ along with $k=|V|$ can be found in Appendix \ref{sec:numvals}).}
    \label{fig:x1x2}
\end{figure}

Let $x_1$ and $x_2$ denote the logit scores of the top 2 candidates output by the \textsc{OCR} model. As introduced in the last chapter (Section \ref{sec:postproc_impl}), I now aim to find characters likely to have been predicted incorrectly by the \textsc{OCR} model by setting a threshold $t$ for the difference between $x_1$ and $x_2$. Any \textsc{OCR} prediction where $x_1-x_2<t$ is treated as likely to be incorrect and is passed on to the correction step. This step works by having a pre-trained \textsc{BERT} model\footnote{cf. the footnote on page \pageref{tab:bert_test}} re-predict the character from the top $k$ \textsc{OCR} candidates. If $x_1-x_2>t$, the \textsc{OCR} prediction is assumed to be correct and will serve as part of the context necessary for the language model (\textsc{LM}). This section will deal with finding suitable values for $t$ and $k$ and present results.

First, I check the assumption that $x_1-x_2$ is smaller if the top candidate is not correct ($\neq$ the gold label). Fig. \ref{fig:x1x2} (a) and (b) show histograms of the values of $x_1-x_2$ for every character in the validation set (bin size on the x-axis: 0.2). While the different scaling of the y-axis has to be taken into account, it is evident that the above assumption holds, which is congruent with the intuition that correct predictions are higher in confidence---or less formally, the model is ``surer" about its prediction when it's correct than when it's wrong. Supporting this finding, manual evaluation revealed that among wrong predictions with high confidence there is a considerable amount of annotation errors---i.e. the model predicted the correct character but the prediction was classified as wrong due to an incorrect gold label. However, there are only 227 cases (1\% of the validation set) where for a wrong prediction $x_1-x_2>3$.

Depending on the threshold $t$, a differently sized proportion of the \textsc{OCR} output is re-evaluated by the \textsc{LM}. The extremal cases are trivial:

\begin{enumerate}
\setlength\itemsep{-0.3em}
    \item $t=0 \implies$ The entire \textsc{OCR} output is assumed to be correct, the \textsc{LM} is never used.
    \item $t\geq \max_{i,j} (x_i-x_j) \implies$ The entire \textsc{OCR} output is re-evaluated by the \textsc{LM}:\vspace{-1mm}
    \begin{enumerate}
    \setlength\itemsep{-0.3em}
        \item $k=1 \implies$ The \textsc{LM} only gets one candidate to choose from, equivalent to 1.
        \item $k=|V|$ ($k$ is equal to the \textsc{LM}'s vocabulary size) $\implies$ Every character is re-evaluated, the final accuracy would be somewhere near $p(1)$ in Table \ref{tab:bert_test}.
    \end{enumerate}
\end{enumerate}
Systematically testing different values of $t$ and $k$ with $t \in [0, 0.5, \dots, 10]$ and $k \in [0, 1, \dots, 18]$, I obtain the results shown in Fig. \ref{fig:x1x2} (c). For comparison, the original \textsc{OCR} accuracy of 97.63\% is marked as a horizontal line. It is evident that within certain intervals of $t$ and $k$, the \textsc{LM} indeed lowers the error on the validation set, i.e. post-processing is successful. Optimal values for $t$ and $k$ are obtained by the following observations:
\begin{itemize}
\setlength\itemsep{-0.3em}
    \item[$t$:] Independent of $k$, the best performance is consistently obtained for $2.0~\leq~t~\leq~3.0$, which is in line with the observations in Fig. \ref{fig:x1x2} (a) and (b) where this is the region that intuitively best separates correct from incorrect predictions. As within this interval there is no clear tendency of a specific maximum (cf. also Appendix \ref{sec:numvals}), I settle with the midpoint of \underline{\smash{$t=2.5$}}.
    \item[$k$:] The best performance is obtained for \underline{\smash{$k=7$}}, albeit by a very slight margin over any $3 \leq k \leq 18$. As for lower and higher $k$: At $k=2$ the \textsc{LM} correction does improve performance over the basic \textsc{OCR} model, but while in wrong predictions the correct candidate is often in second place (cf. Table \ref{tab:wrongpreds} on page \pageref{tab:wrongpreds}), this misses out on the cases where it is further back. Within $3 \leq k \leq 18$, the \textsc{LM} reveals its full potential: The small number of visually similar characters are not likely to be semantically similar and thus easily identifiable by the context-aware \textsc{LM}. For $k>7$, the top accuracy value ($2.0 \leq t \leq 3.0$) slowly decreases, presumably because given greater choice, the \textsc{LM} becomes ever more likely to find a semantically fitting (but incorrect) candidate, until more and more initially correct \textsc{OCR} predictions are ``mis-corrected" and the overall accuracy drops below the initial \textsc{OCR} accuracy. At $k = |V|$, no improvement over the \textsc{OCR} model is made for any $t$ (cf. \ref{sec:numvals}).
\end{itemize}


\section{Final Results on the Test Set}

\begin{table}[b]
\hspace*{-0.08\linewidth}
\begin{minipage}{0.6\linewidth}
    \centering
    \begin{tabular}{lcc}
    \toprule
        & val. set & test set \\ \midrule
        only \textsc{OCR} w/o pre-training & 96.54 & 95.49 \\
        only \textsc{OCR} w/ pre-training  & 97.63 & 96.95 \\
        \textsc{OCR} w/ pre-training + \textsc{LM} & \textbf{98.05} & \textbf{97.44}  \\
        \bottomrule
    \end{tabular}
    \caption[Classification accuracy on validation and test set]{Classification accuracy (\%) on validation and test set}
    \label{tab:final_acc}%
\end{minipage}%
\hspace*{5mm}%
\begin{minipage}{0.5\linewidth}%
    {\renewcommand{\arraystretch}{1.2}
    \centering
    \begin{tabular}{rr|>{\centering\arraybackslash}p{16mm}>{\centering\arraybackslash}p{16mm}}
         & & \multicolumn{2}{c}{after applying \textsc{LM}:} \\
         & & right & wrong \\ \hline
        \multirow{2}{*}{\rotatebox{90}{\parbox{15mm}{\centering \textsc{OCR} result:}}} & right & 292 & 82 \\
         & wrong & 174 & 54+165* \\
    \end{tabular}
    \caption[Confusion table for the \textsc{LM} correction step]{Confusion table for the \textsc{LM} correction step (*54 left unchanged, 165 changed to another wrong character)}
    \label{tab:lmstats}
}
\end{minipage}
\end{table}

Resulting from the observations described in the last section, the best model (as of Table \ref{tab:ocr_results}) was evaluated on the test set using $t=2.5$ and $k=7$. Of the 22,377 images in the test set, 96.95\% were correctly identified by the \textsc{OCR} model. 767 characters were passed to the \textsc{LM} for re-evaluation (see paragraph below), whereafter a total accuracy of 97.44\% is achieved. As becomes apparent in Table \ref{tab:final_acc}, these values are lower than on the validation set, where for $t=2.5, k=7$ accuracies of 97.63\% and 98.05\% were attained, respectively. Presumably, the crops in the test set happened to originate from less clearly printed sections of the newspaper. Nevertheless, the overall correction rate of the \textsc{LM} step is consistent between validation and test set, with error reduction rates of 18.1\% (validation set) and 16.1\% (test set).

Finally, Table \ref{tab:lmstats} gives additional insight into what happens at the \textsc{LM} correction step: Of the \textsc{OCR} output passed to the \textsc{LM}, correct and incorrect characters nearly make up one half each (374 right, 393 wrong). After applying the \textsc{LM}, this ration shifts to about 60--40 (466 correct, 311 incorrect). Precisely, 44\% of wrong \textsc{OCR} output passed to the \textsc{LM} is corrected, but also 22\% of the characters correctly predicted by the \textsc{OCR} model are changed to incorrect ones. Though ultimately, positive changes outweigh negative ones, the absolute amount of correct \textsc{OCR} output ``botched" by the \textsc{LM} leaves room for improvement.
