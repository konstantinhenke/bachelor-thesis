import pandas as pd

df = pd.read_csv('correction_results.csv')
for k,sub_df in df.groupby('k'):
    if k<19:
        print(r"\begin{minipage}{.2\linewidth}")
        print(r"\textbf{k =", str(k) + "}:\\\\[0.8em]")
        print(sub_df[:21].to_latex(index=False,columns=['t','bert']))
        print(r"\end{minipage}%")
