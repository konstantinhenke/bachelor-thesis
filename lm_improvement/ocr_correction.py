import sys, os
sys.path.append("../classifier")
import numpy as np
import cv2
import json
import models
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

from transformers import (
  BertTokenizerFast,
  BertForMaskedLM,
  pipeline
)

# def print_top_k_results(seq,pipe):
#     for i,char in enumerate(seq):
#         masked = seq[:i] + tokenizer.mask_token + seq[i+1:]
#         # print(masked.replace(tokenizer.mask_token,"＿"))
#         preds = pipe(masked)
#         out = [(pred["token_str"],round(pred["score"],2)) for pred in preds]
#         try:
#             predicted_index = [pred["token_str"] for pred in preds].index(char)
#         except ValueError:
#             predicted_index = None
#         print(f" {char}: {predicted_index} – ", end="")
#         for x in out:
#             print(x[0], x[1], end=" ")
#         print()

def load_image_for_ocr_model(path): # doing the same thing as in classifier/dataset.py
    img = cv2.imread(path,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (224,224), interpolation=cv2.INTER_CUBIC)
    img = cv2.bitwise_not(img)
    img = transforms.functional.to_tensor(img).view(1,1,224,224) # to get torch.Size([1, 1, 224, 224]) needed for the model
    return img

# def show(name,img):
#     cv2.imshow(name,img)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()


if __name__ == "__main__":

    # === SET UP OCR === #
    ocr_model        = "../classifier/googlenet-b4-l0.001-on-train_data-afterPretrainingOnAllFonts-26.pth.tar"
    path_to_crops    = "../classifier/annotated_crops/"
    path_to_val_data = "../classifier/val_data/"
    val_crop_indices = {file.split("-")[1] for file in os.listdir(path_to_val_data)} # {'001', '003', '005', '011', '012', '018', ...}
    val_txt_paths    = {f"{path_to_crops}{val_crop_idx}.txt" for val_crop_idx in val_crop_indices}
    glyph_dict       = json.load(open("../classifier/glyph_dict.json"))

    sort_by_column_and_idx = lambda x:(int(x[6:].split("-")[0]),int(x[6:].split("-")[1].strip(".png")))
    img_lists_per_crop = [sorted(
                            filter(
                              lambda x:x[2:5]==idx,
                              os.listdir(path_to_val_data)
                            ),
                            key=sort_by_column_and_idx
                          ) for idx in val_crop_indices]
    # [['本-469-1-2.png', '月-469-1-3.png', ...], ['沿-346-1-1.png', '東-346-1-2.png', '江-346-1-3.png'], ...]

    NUM_CLASSES = len(glyph_dict)
    label2unicode = {v:k for k,v in glyph_dict.items()}

    # set device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(f'device: {device}')

    # load model
    model = models.GoogleNetModel(num_classes=NUM_CLASSES)
    model.load_state_dict(torch.load(ocr_model,map_location=torch.device(device)))

    # evaluate
    model.to(device)
    model.eval()

    # === SET UP LANGUAGE MODEL === #

    bert_model = BertForMaskedLM.from_pretrained('ckiplab/bert-base-chinese')
    tokenizer = BertTokenizerFast.from_pretrained('bert-base-chinese')
    pipe = pipeline("fill-mask", model=bert_model, tokenizer=tokenizer)

    bert_model.eval()

    with torch.no_grad():

        # for k in range(2,19):
        for k in [7]:
            # for t in np.arange(0,10.5,0.5):
            for t in [2.5]:

                crop_count = 0
                correct_count_ocr  = 0
                correct_count_lm   = 0
                total_char_count   = 0

                # more detailed stats
                right_ocr_left_unchanged = 0
                wrong_ocr_left_unchanged = 0
                wrong_ocr_changed_but_wrong = 0
                wrong_ocr_corrected = 0
                right_ocr_botched   = 0

                total_char_count_save = 0

                for img_list in img_lists_per_crop:
                    # one img_list looks like this: ['本-469-1-2.png', '月-469-1-3.png', ...]

                    # first OCR all of it
                    pred_seq = []
                    gold_seq = []
                    low_confidence_top_k_candidates = {} # idx : top k OCR candidates
                    for idx,file_name in enumerate(img_list):
                        img_tensor = load_image_for_ocr_model(os.path.join(path_to_val_data,file_name))
                        img_tensor = img_tensor.cuda() if device == "cuda" else img_tensor

                        preds = model(img_tensor)
                        top_k = torch.topk(preds,k)

                        goldlabel_char = file_name[0]
                        top_k_chars = [chr(int(label2unicode[int(idx)],16)) for idx in top_k.indices[0]]
                        top_k_scores = [val.item() for val in top_k.values[0]]

                        if top_k_scores[0]-top_k_scores[1] < t: # low confidence candidate
                            low_confidence_top_k_candidates[idx] = top_k_chars

                        pred_seq.append(top_k_chars[0]) # use top-1 predictions for BERT later
                        gold_seq.append(goldlabel_char) # to check if accuracy improves with LM

                        # to plot histograms later
                        # with open("top1top2diff.txt", "a") as g:
                        #     if goldlabel_char == top_k_chars[0]: # correct prediction
                        #         g.write(f"y {top_k_scores[0]-top_k_scores[1]}\n")
                        #     else:
                        #         g.write(f"- {top_k_scores[0]-top_k_scores[1]}\n")

                    pred_seq = "".join(pred_seq)
                    gold_seq = "".join(gold_seq)
                    correct_count_ocr += np.count_nonzero([x==y for x,y in zip(pred_seq,gold_seq)])

                    # correct sequence using LM at low confidence indices

                    if not len(pred_seq) > 512: # context window size!
                        for idx,top_k_candidates in low_confidence_top_k_candidates.items():
                            masked = pred_seq[:idx] + tokenizer.mask_token + pred_seq[idx+1:]
                            preds = pipe(masked,targets=top_k_candidates)

                            # detailed stats about corrections and miscorrectiosn
                            ocr_pred = pred_seq[idx]
                            lm_pred  = preds[0]['token_str']
                            goldchar = gold_seq[idx]

                            if ocr_pred == goldchar:
                                if lm_pred == goldchar:
                                    right_ocr_left_unchanged += 1
                                    total_char_count_save += 1
                                else:
                                    right_ocr_botched += 1
                                    total_char_count_save += 1
                            else: # ocr_pred != goldchar:
                                if lm_pred == goldchar:
                                    wrong_ocr_corrected += 1
                                    total_char_count_save += 1
                                elif ocr_pred == lm_pred:
                                    wrong_ocr_left_unchanged += 1
                                    total_char_count_save += 1
                                else: #lm_pred != goldchar and ocr_pred != lm_pred
                                    wrong_ocr_changed_but_wrong += 1
                                    total_char_count_save += 1

                            # replace char predicted by lm
                            if len(preds[0]["token_str"]) == 1: # make sure it's not e.g. [UNK]
                                pred_seq = pred_seq[:idx] + preds[0]["token_str"] + pred_seq[idx+1:]
                    else:
                        print(img_list[0][2:5], "is longer than 512 tokens") # crop number

                    correct_count_lm += np.count_nonzero([x==y for x,y in zip(pred_seq,gold_seq)])
                    total_char_count += len(gold_seq)

                    crop_count += 1

                with open("correction_results.csv", "a") as f:
                    f.write(f"{k}, {t}, {round(correct_count_ocr/total_char_count*100,2)}, {round(correct_count_lm/total_char_count*100,2)}\n")
                    f.write(f"total_char_count: {total_char_count_save} == {total_char_count}\n=======\n")
                    f.write(f"right_ocr_left_unchanged: {right_ocr_left_unchanged}\n")
                    f.write(f"wrong_ocr_left_unchanged: {wrong_ocr_left_unchanged}\n")
                    f.write(f"wrong_ocr_changed_but_wrong: {wrong_ocr_changed_but_wrong}\n")
                    f.write(f"wrong_ocr_corrected: {wrong_ocr_corrected}\n")
                    f.write(f"right_ocr_botched: {right_ocr_botched}\n")

    # # checking LMs performance on GT:
    # for txt in val_txt_paths:
    #     print("="*20)
    #     print(txt)
    #     with open(txt) as f:
    #         lines = [line.strip()
    #                      .rstrip("<lb/>")
    #                      .lstrip("e")
    #                      .replace("cc","，")
    #                      .replace("&gaiji;",tokenizer.unk_token)
    #                      .replace("　","")
    #                 for line in f.readlines()]
    #         seq = "".join(lines)
    #         print_top_k_results(seq,pipe)
    # print_top_k_results(, pipe)
