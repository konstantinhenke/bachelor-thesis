import os
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
sns.set_theme(style="darkgrid")

with open("top1top2diff.txt") as f:
    lines = f.readlines()

scores_of_correct_preds = []
scores_of_wrong_preds = []
bins = np.arange(0,25,0.2)

for line in lines:
    if line.startswith("y"):
        scores_of_correct_preds.append(float(line.split()[1]))
    else:
        scores_of_wrong_preds.append(float(line.split()[1]))
#
# print(len(list(filter(lambda x:x>3,scores_of_wrong_preds))))

fig, (ax1, ax2, ax3, ax4) = plt.subplots(4,1,figsize=(10,12),sharex=True)
ax1.hist(scores_of_correct_preds,color="royalblue",bins=bins)
ax2.hist(scores_of_wrong_preds,color="cornflowerblue",bins=bins)
ax1.set_ylabel('(a) correct predictions')
ax2.set_ylabel('(b) wrong predictions')
ax2.set_xlabel('logit difference between top 1 and top 2 candidate')

cmap = plt.cm.get_cmap('rainbow',20)

df = pd.read_csv('correction_results_selected.csv')
for k,sub_df in df.groupby('k'):
    ax3.scatter(sub_df['t'],sub_df['bert'],label=f"k = {k}",c=cmap(k))
ax3.plot(np.arange(0,10.5,0.5),21*[97.63],label='OCR')
ax3.set_xlabel('t')
ax3.set_ylabel("(c) val. accuracy after OCR correction")
ax3.legend()

df4 = pd.read_csv('detailed_stats.csv')
df4["ww"] = df4["ww"] + df4["wsw"]

df4["sum"] = df4['rr'] + df4['ww'] + df4['wr'] + df4['rw']
print(df4["sum"])
df4['r → r'] = df4['rr']/df4['sum']*100
df4['w → w'] = df4['ww']/df4['sum']*100
df4['w → r'] = df4['wr']/df4['sum']*100
df4['r → w'] = df4['rw']/df4['sum']*100
df4.plot.area(x='t', y=['r → w','w → r'], ax=ax4, stacked='True')

plt.xlim(-0.2,22)
plt.savefig("histograms.png", dpi=300, bbox_inches='tight')

# # only scatterplot:
# cmap = plt.cm.get_cmap('rainbow',20)
#
# df = pd.read_csv('correction_results_selected.csv')
# print(df)
# for k,sub_df in df.groupby('k'):
#     ax = sub_df.plot(kind='scatter',x='t',y='bert',label=f"k = {k}",c=cmap(k),ax=plt.gca())
# ax.plot(np.arange(0,10.5,0.5),21*[97.63],label='OCR')
# ax.set_ylabel("val. accuracy after OCR correction")
# plt.show()
