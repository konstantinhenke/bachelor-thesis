import torch
import sys
import numpy as np

# from transformers import GPT2Tokenizer, GPT2LMHeadModel

from transformers import (
  BertTokenizerFast,
  BertForMaskedLM,
  pipeline
)

def unmask(seq, model, tokenizer, k=5):
    input = tokenizer.encode(seq, return_tensors="pt")
    mask_token_index = torch.where(input == tokenizer.mask_token_id)[1]

    token_logits = model(input)[0]
    mask_token_logits = token_logits[0, mask_token_index, :]

    top_k = torch.topk(mask_token_logits, k, dim=1)
    top_k_scores    = top_k.values[0].tolist()
    top_k_token_ids = top_k.indices[0].tolist()
    top_k_tokens = [tokenizer.decode([token]) for token in top_k_token_ids]

    return (top_k_tokens,top_k_scores)

if __name__ == "__main__":

    k = 10

    model = BertForMaskedLM.from_pretrained('ckiplab/bert-base-chinese')
    tokenizer = BertTokenizerFast.from_pretrained('bert-base-chinese')
    unmasker = pipeline("fill-mask", model=model, tokenizer=tokenizer, top_k=k)

    model.eval()
    seq = "我很喜歡他。"

    # comparing unmasker (using pipeline) and unmask (manually tokenizing and putting through model)
    for i,char in enumerate(seq):
        masked = seq[:i] + tokenizer.mask_token + seq[i+1:]
        # print(f'\\trad{{{str.replace(masked,tokenizer.mask_token,"＿")}}}', end=" & ")
        print(masked.replace(tokenizer.mask_token,"＿"))

        # using unmasker
        preds = unmasker(masked)
        out = [(pred["token_str"],round(pred["score"],2)) for pred in preds]
        print(" ", char, ": ", end="")
        for x in out:
            # print(f"\\trad{{{x[0]}}} & {x[1]} & ", end="")
            print(x[0], x[1], end="")
        print()

        # using unmask
        top_k_tokens,top_k_scores = unmask(masked, model, tokenizer, k=k)
        print(" ", char, ": ", end="")
        for j in range(k):
            print(top_k_tokens[j], round(top_k_scores[j],2), end="")
        print()
