# Bachelor Thesis

This repository contains the code used for my bachelor thesis, which itself is located at thesis/thesis.pdf

For more recent versions and documentation, please refer the associated ECPO [Github repository](https://github.com/exc-asia-and-europe/ecpo-fulltext-experiments) and the linked repositories there.
